# parent image
FROM golang:1.17-alpine AS builder

WORKDIR /src/app

# installation instructions
COPY go.mod go.sum ./
RUN apk add --no-cache git \
    && go mod download

COPY . .
RUN go install

FROM alpine:latest
LABEL maintainer "youtous <contact@youtous.me>"

EXPOSE 1323

# the default command to run for container
ENTRYPOINT ["deepcube-example"]

RUN apk add --no-cache ca-certificates curl

# (optional) permit to check the health of the container
# https://scoutapm.com/blog/how-to-use-docker-healthcheck
HEALTHCHECK CMD curl --silent --fail http://localhost:1234 > /dev/null || exit 1

COPY --from=builder /go/bin/deepcube-example /usr/local/bin/
